public class UsingExceptions{
	public static void main(String[] args) {
		try{
			throwException;
		}catch (Exception exception){
			System.err.println("Exception handled in main");
		}doesNotthrowException();
	}
	public static void throwException() throws Exception{
		try{
			System.out.println("Method throwException");
			throws new Exception();
		}finaly{
			System.err.println("Finally executed in throwException")
		}
	}
	public static void doesNotthrowException (){
		try{
			System.out.println("Method doesNotthrowException");
		}catch (Exception exception){
			System.err.println("Finally executed in doesNotthrowException");
		}finally{
			System.err.println("End of method doesNotthrowException");
		}
	}
}